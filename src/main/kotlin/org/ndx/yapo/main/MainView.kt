package org.ndx.yapo.main

import tornadofx.View
import javafx.scene.layout.VBox

class MainView : View(){
	override val root = VBox()
}