package org.ndx.yapo.main

import javafx.application.Application

fun main(args: Array<String>) {
  Application.launch(MainApp::class.java, *args)
}